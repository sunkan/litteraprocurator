<?php

namespace LitteraProcurator\Workers;

use Beanstalk\Client;
use LitteraProcurator\TemplateObject;
use LitteraProcurator\TemplateStorageInterface;
use Psr\Log\LoggerInterface;
use SendGrid;

class Mailer
{
    private $keyCache = [];
    private $subs = [
        '%account_id%' => 'accounts.id',
        '%email%' => 'accounts.email',
        '%last_login%' => 'accounts.last_login',
        '%first_name%' => 'profiles.first_name',
        '%last_name%' => 'profiles.last_name',
        '%name%' => 'profiles.name',
        '%birthday%' => 'profiles.birthdate',
    ];
    private $tube;

    protected $logger;
    protected $sendGrid;
    protected $beanstalkdClient;
    protected $templateStorage;

    public function __construct(
        Client $beanstalkdClient,
        LoggerInterface $logger,
        SendGrid $sendGrid,
        TemplateStorageInterface $templateStorage = null,
        $tube = 'lp_mailer'
    ) {
        $this->tube = $tube;
        $this->templateStorage = $templateStorage;
        $this->logger = $logger;
        $this->sendGrid = $sendGrid;
        $this->beanstalkdClient = $beanstalkdClient;
    }

    public function stats()
    {
        $this->beanstalkdClient->connect();
        return $this->beanstalkdClient->statsTube($this->tube);
    }

    public function run($tube = false)
    {
        if ($tube) {
            $this->tube = $tube;
        }

        $this->logger->debug("Connecting ...");
        if (!$this->beanstalkdClient->connect()) {
            $this->logger->error("Failed to connect to beanstalkd server");
            return;
        }

        $this->logger->debug("Subscribing to " . $this->tube);

        $this->beanstalkdClient->watch($this->tube);

        $this->logger->debug("Start listening");
        while (true) {
            $job = $this->beanstalkdClient->reserve(); // Block until job is available.
            $data = json_decode($job['body'], true);

            // Processing of the job...
            $result = $this->_process($data, $job['id']);
            if ($result) {
                $this->logger->info("Deleting job");
                $this->beanstalkdClient->delete($job['id']);
            } else {
                $this->logger->notice('Job fail. Bury job', [
                    'jobId' => $job['id']
                ]);
                $this->beanstalkdClient->bury($job['id'], 10);
            }
        }
    }

    private function _process($data, $jobId)
    {
        $email = new Email();
        $keys = [];
        $subData = [];
        foreach ($data as $payload) {
            $method = $payload[0];
            $args = $payload[1];
            if ($method == 'setTemplate') {
                if (!$this->templateStorage) {
                    $this->logger->error('Template storage not initialized', [
                        'jobId' => $jobId
                    ]);
                    return false;
                }
                $template = $this->templateStorage->getTemplate($args[0]);
                if (!($template instanceof TemplateObject)) {
                    $this->logger->error('Template missing', [
                        'template' => $args[0],
                        'jobId' => $jobId
                    ]);
                    return false;
                }
                $keys = $keys + $template->getKeys();
                $email->setSubject($template->getSubject());
                $email->setText($template->getText());
                $email->setHtml($template->getHtml());
            } elseif ($method == 'addAccount') {
                $accountInfo = [
                    'accounts' => $args[0],
                    'profiles' => $args[1],
                ];
                $email->addTo($accountInfo['accounts']['email']);
                if ($keys) {
                    foreach ($keys as $key) {
                        if ($this->subs[$key]) {
                            list($table, $field) = explode('.', $this->subs[$key]);
                            $subData[$key][] = $accountInfo[$table][$field];
                        }
                    }
                }
            } elseif (in_array($method, ['setHtml', 'setSubject'])) {
                if (strpos($args[0], '%') !== false) {
                    $keys = $keys + $this->_getKeys($args[0], md5($args[0]));
                }
            }
            if (method_exists($email, $method)) {
                call_user_func_array([$email, $method], $args);
            }
        }
        if (count($subData)) {
            foreach ($subData as $key => $value) {
                $email->addSubstitution($key, $value);
            }
        }
        /** @var SendGrid\Response $response */
        $response = $this->sendGrid->client->mail()->send()->post($email->getMail());
        if ($response->statusCode() >= 200 && $response->statusCode() < 400) {
            $this->logger->debug('Email sent: '.$response->code);
            $this->logger->info('email success', [
                'jobId' => $jobId,
            ]);
            return true;
        } else {
            $this->logger->error("Email sent error: ", [
                'jobId' => $jobId,
                'code' => $response->statusCode(),
                'message' => $response->body(),
            ]);
            return false;
        }
    }

    private function _getKeys($text, $cacheId)
    {
        if (!isset($this->keyCache[$cacheId])) {
            $rs = preg_match_all('/(\%\w+\%)/', $text, $matches);
            if ($rs) {
                $this->keyCache[$cacheId] = $matches[1];
            } else {
                $this->keyCache[$cacheId] = [];
            }
        }
        return $this->keyCache[$cacheId];
    }
}
