<?php

namespace LitteraProcurator\Workers;

use SendGrid\Attachment;
use SendGrid\Content;
use SendGrid\Email as EmailAddress;
use SendGrid\Mail;
use SendGrid\Personalization;
use SendGrid\ReplyTo;

class Email
{
    /**
     * @var \SendGrid\Email
     */
    private $fromEmail;

    public function __construct()
    {
        $this->mail = new Mail();
        $this->personalization = new Personalization();
    }

    public function addTo($email, $name = null)
    {
        $this->personalization->addTo($this->createEmailAddress($email, $name));
        return $this;
    }

    public function setTos(array $emails)
    {
        foreach ($emails as $email) {
            if (is_array($email)) {
                $this->addTo($email[0], $email[1]);
            } else {
                $this->addTo($email);
            }
        }
        return $this;
    }

    public function addCc($email, $name = null)
    {
        $this->personalization->addCc($this->createEmailAddress($name ?: '', $email));
        return $this;
    }

    public function setFrom($email, $name = null)
    {
        if (!$this->fromEmail) {
            $this->fromEmail = new EmailAddress('', $email);
        } else {
            $this->fromEmail->setEmail($email);
        }
        return $this;
    }

    public function setFromName($name)
    {
        if (!$this->fromEmail) {
            $this->fromEmail = new EmailAddress($name, '');
        } else {
            $this->fromEmail->setName($name);
        }
        return $this;
    }

    public function setReplyTo($email)
    {
        if (!($email instanceof ReplyTo)) {
            $email = new ReplyTo($email);
        }
        $this->mail->setReplyTo($email);
        return $this;
    }

    public function setCcs(array $emailList)
    {
        foreach ($emailList as $email) {
            if (is_array($email)) {
                $this->addCc($email[0], $email[1]);
            } else {
                $this->addCc($email);
            }
        }
        return $this;
    }

    public function setBccs(array $emailList)
    {
        foreach ($emailList as $email) {
            if (is_array($email)) {
                $this->addBcc($email[0], $email[1]);
            } else {
                $this->addBcc($email);
            }
        }
        return $this;
    }

    public function addBcc($email, $name = null)
    {
        $this->personalization->addBcc($this->createEmailAddress($name, $email));
        return $this;
    }

    private function createEmailAddress($email, $name = null)
    {
        if ($email instanceof EmailAddress) {
            return $email;
        }
        return new EmailAddress($name, $email);
    }

    public function setSubject($subject)
    {
        $this->mail->setSubject($subject);
        return $this;
    }

    public function setText($text)
    {
        $content = new Content("text/plain", $text);
        return $this->addContent($content);
    }

    public function setHtml($html)
    {
        $content = new Content("text/html", $html);
        return $this->addContent($content);
    }

    public function addContent(Content $content)
    {
        $this->mail->addContent($content);
        return $this;
    }

    public function setSendAt($timestamp)
    {
        $this->personalization->setSendAt($timestamp);
        return $this;
    }

    public function addAttachment(Attachment $file)
    {
        $this->mail->addAttachment($file);
        return $this;
    }

    public function setCategory($category)
    {
        $this->mail->addCategory($category);
        return $this;
    }

    public function addCategory($category)
    {
        $this->mail->addCategory($category);
        return $this;
    }

    public function setSubstitutions($keyValuePairs)
    {
        foreach ($keyValuePairs as $key => $value) {
            $this->addSubstitution($key, $value);
        }
        return $this;
    }

    public function addSubstitution($fromValue, $toValues)
    {
        $this->stack[] = ['addSubstitution', [$fromValue, $toValues]];
        if (is_array($toValues)) {
            $this->personalization->addSubstitution($fromValue, $toValues[0]);
        } else {
            $this->personalization->addSubstitution($fromValue, $toValues);
        }
        return $this;
    }

    public function setSections(array $keyValuePairs)
    {
        foreach ($keyValuePairs as $key => $value) {
            $this->addSection($key, $value);
        }
        return $this;
    }

    public function addSection($fromValue, $toValue)
    {
        $this->mail->addSection($fromValue, $toValue);
        return $this;
    }

    public function setHeaders($keyValuePairs)
    {
        foreach ($keyValuePairs as $key => $value) {
            $this->addHeader($key, $value);
        }
        return $this;
    }

    public function addHeader($key, $value)
    {
        $this->personalization->addHeader($key, $value);
        return $this;
    }

    public function getMail()
    {
        $this->mail->setFrom($this->fromEmail);
        $this->mail->addPersonalization($this->personalization);
        return $this->mail;
    }
}
