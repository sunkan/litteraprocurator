<?php

namespace LitteraProcurator;

use Beanstalk\Client as BeanstalkdClient;

class EmailFactory
{
    protected $beanstalkdClient;
    protected $tube;

    public function __construct(BeanstalkdClient $client, $tube = 'lp_mailer')
    {
        $this->beanstalkdClient = $client;
        $this->tube = $tube;
    }

    public function createEmail()
    {
        return new Email($this->beanstalkdClient, $this->tube);
    }
}
