<?php

namespace LitteraProcurator;

interface TemplateStorageInterface
{
    /**
     * @param mixed $key
     * @return TemplateObject
     */
    public function getTemplate($key);
}