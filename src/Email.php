<?php

namespace LitteraProcurator;

use Beanstalk\Client as BeanstalkdClient;
use JsonSerializable;

class Email implements JsonSerializable
{
    protected $beanstalkdClient;
    protected $defaultTube;
    public $stack = [];

    public function __construct(BeanstalkdClient $client, $defaultTube = 'lp_mailer')
    {
        $this->beanstalkdClient = $client;
        $this->defaultTube = $defaultTube;
    }

    public function addAccount($account, $profile)
    {
        if (!is_array($account)) {
            if ($account instanceof JsonSerializable) {
                $account = $account->jsonSerialize();
            } elseif (method_exists($account, 'toArray')) {
                $account = $account->toArray();
            }
        }
        if (!is_array($profile)) {
            if ($profile instanceof JsonSerializable) {
                $profile = $profile->jsonSerialize();
            } elseif (method_exists($profile, 'toArray')) {
                $profile = $profile->toArray();
            }
        }

        $this->stack[] = ['addAccount', [$account, $profile]];
        return $this;
    }

    public function setTemplate($id)
    {
        $this->stack[] = ['setTemplate', [(int) $id]];
        return $this;
    }

    public function addTo($email, $name = null)
    {
        $this->stack[] = ['addTo', [$email, $name]];
        return $this;
    }

    public function setTos(array $emails)
    {
        $this->stack[] = ['setTos', [$emails]];
        return $this;
    }

    public function setFrom($email)
    {
        $this->stack[] = ['setFrom', [$email]];
        return $this;
    }

    public function setFromName($name)
    {
        $this->stack[] = ['setFromName', [$name]];
        return $this;
    }

    public function setReplyTo($email)
    {
        $this->stack[] = ['setReplyTo', [$email]];
        return $this;
    }

    public function setCc($email)
    {
        $this->stack[] = ['setCc', [$email]];
        return $this;
    }

    public function setCcs(array $emailList)
    {
        $this->stack[] = ['setCcs', [$emailList]];
        return $this;
    }

    public function addCc($email)
    {
        $this->stack[] = ['addCc', [$email]];
        return $this;
    }

    public function setBcc($email)
    {
        $this->stack[] = ['setBcc', [$email]];
        return $this;
    }

    public function setBccs(array $emailList)
    {
        $this->stack[] = ['setBccs', [$emailList]];
        return $this;
    }

    public function addBcc($email)
    {
        $this->stack[] = ['addBcc', [$email]];
        return $this;
    }

    public function setSubject($subject)
    {
        $this->stack[] = ['setSubject', [$subject]];
        return $this;
    }

    public function setDate($date)
    {
        $this->stack[] = ['setDate', [$date]];
        return $this;
    }

    public function setText($text)
    {
        $this->stack[] = ['setText', [$text]];
        return $this;
    }

    public function setHtml($html)
    {
        $this->stack[] = ['setHtml', [$html]];
        return $this;
    }

    public function setSendAt($timestamp)
    {
        $this->stack[] = ['setSendAt', [$timestamp]];
        return $this;
    }

    public function setSendEachAt(array $timestamps)
    {
        $this->stack[] = ['setSendEachAt', [$timestamps]];
        return $this;
    }

    public function addSendEachAt($timestamp)
    {
        $this->stack[] = ['addSendEachAt', [$timestamp]];
        return $this;
    }

    public function setAttachments(array $files)
    {
        $this->stack[] = ['setAttachments', [$files]];
        return $this;
    }

    public function setAttachment($file, $customFilename = null, $cid = null)
    {
        $this->stack[] = ['setAttachment', [$file, $customFilename, $cid]];
        return $this;
    }

    public function addAttachment($file, $customFilename = null, $cid = null)
    {
        $this->stack[] = ['addAttachment', [$file, $customFilename, $cid]];
        return $this;
    }

    public function setCategories($categories)
    {
        $this->stack[] = ['setCategories', [$categories]];
        return $this;
    }

    public function setCategory($category)
    {
        $this->stack[] = ['setCategory', [$category]];
        return $this;
    }

    public function addCategory($category)
    {
        $this->stack[] = ['addCategory', [$category]];
        return $this;
    }

    public function setSubstitutions($keyValuePairs)
    {
        $this->stack[] = ['setSubstitutions', [$keyValuePairs]];
        return $this;
    }

    public function addSubstitution($fromValue, array $toValues)
    {
        $this->stack[] = ['addSubstitution', [$fromValue, $toValues]];
        return $this;
    }

    public function setSections(array $keyValuePairs)
    {
        $this->stack[] = ['setSections', [$keyValuePairs]];
        return $this;
    }

    public function addSection($fromValue, $toValue)
    {
        $this->stack[] = ['addSection', [$fromValue, $toValue]];
        return $this;
    }

    public function setUniqueArgs(array $keyValuePairs)
    {
        $this->stack[] = ['setUniqueArgs', [$keyValuePairs]];
        return $this;
    }

    ## synonym method
    public function setUniqueArguments(array $keyValuePairs)
    {
        $this->stack[] = ['setUniqueArguments', [$keyValuePairs]];
        return $this;
    }

    public function addUniqueArg($key, $value)
    {
        $this->stack[] = ['addUniqueArg', [$key, $value]];
        return $this;
    }

    ## synonym method
    public function addUniqueArgument($key, $value)
    {
        $this->stack[] = ['addUniqueArg', [$key, $value]];
        return $this;
    }

    public function setFilters($filterSettings)
    {
        $this->stack[] = ['setFilters', [$filterSettings]];
        return $this;
    }

    ## synonym method
    public function setFilterSettings($filterSettings)
    {
        $this->stack[] = ['setFilters', [$filterSettings]];
        return $this;
    }

    public function addFilter($filterName, $parameterName, $parameterValue)
    {
        $this->stack[] = ['addFilter', [$filterName, $parameterName, $parameterValue]];
        return $this;
    }

    ## synonym method
    public function addFilterSetting($filterName, $parameterName, $parameterValue)
    {
        $this->stack[] = ['addFilter', [$filterName, $parameterName, $parameterValue]];
        return $this;
    }

    public function setHeaders($keyValuePairs)
    {
        $this->stack[] = ['setHeaders', [$keyValuePairs]];
        return $this;
    }

    public function addHeader($key, $value)
    {
        $this->stack[] = ['addHeader', [$key, $value]];
        return $this;
    }

    public function jsonSerialize()
    {
        return $this->stack;
    }

    public function send($tube = null, $key = false)
    {
        if (is_null($tube)) {
            $tube = $this->defaultTube;
        }
        if ($key) {
            $this->stack[] = ['api_key', $key];
        }
        $client = $this->beanstalkdClient;
        $client->connect();
        $client->useTube($tube);
        return $client->put(
            10, // Give the job a priority of 23.
            0, // Do not wait to put job into the ready queue.
            60, // Give the job 1 minute to run.
            json_encode($this) // The job's body.
        );
    }
}
