<?php

namespace LitteraProcurator;

class TemplateObject
{
    private $keys;
    private $text;
    private $html;
    private $subject;

    public function __construct($subject, $html, $text, array $keys)
    {
        $this->subject = $subject;
        $this->html = $html;
        $this->text = $text;
        $this->keys = $keys;
    }

    /**
     * @return array
     */
    public function getKeys()
    {
        return $this->keys;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }
}
