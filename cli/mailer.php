<?php
require __DIR__.'/../vendor/autoload.php';
session_start();
error_reporting(E_ALL ^ E_STRICT ^ E_NOTICE);
date_default_timezone_set('UTC');

use LitteraProcurator\Workers\Mailer;
use Beanstalk\Client as BeanstalkdClient;
use Psr\Log\NullLogger as Logger;

$worker = new Mailer(
    new BeanstalkdClient(),
    new Logger(),
    new SendGrid(getenv('SENDGRID_API_KEY'))
);
$worker->run();
